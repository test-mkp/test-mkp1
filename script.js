document.addEventListener("DOMContentLoaded", function () {
  const pizzas = document.querySelectorAll('input[name="pizza"]');
  const sizes = document.querySelectorAll('input[name="size"]');
  const toppings = document.querySelectorAll('input[name="topping"]');
  const totalPrice = document.getElementById("total-price");

  let pizzaPrice = 0;
  let sizePrice = 0;
  let toppingsPrice = 0;
  let pizzaSelected = false; //ini untuk validasi apakah pizza sudah dipilih atau belum

  //kita update price dengan menjumlahkan pizzaPrice,sizePrice dan toppingsPrice
  const updatePrice = () => {
    let total = pizzaPrice + sizePrice + toppingsPrice;
    totalPrice.textContent = `$${total}`;
  };

  // Fungsi untuk mereset size ke default (medium)
  const resetSize = () => {
    sizes.forEach((size) => {
      if (size.id === "medium") {
        size.checked = true;
        sizePrice = parseInt(size.value);
      } else {
        size.checked = false;
      }
    });
  };

  //kita buat fungsi untuk mereset topping
  const resetToppings = () => {
    toppings.forEach((topping) => {
      topping.checked = false;
      topping.disabled = true;
    });
    toppingsPrice = 0;
  };

  //disini kita menerima data topping yang diperbolehkan sesuai dengan pizza yang kita pilih lalu kita enable
  const enableAllowedToppings = (allowedToppings) => {
    allowedToppings.split(",").forEach((toppingId) => {
      document.getElementById(toppingId).disabled = false;
    });
  };

  //   disini kita menambah event listener u/ pilihan pizza, jadi ketika pizza kita pilih(checked) maka pizzaPrice akan diperbarui sesuai value dari pizza yg kita pilih, dan topping akan tereset, lalu kita enable topping yang awalnya disable, dan harganya diupdate
  pizzas.forEach((pizza) => {
    pizza.addEventListener("change", function () {
      if (this.checked) {
        pizzaSelected = true;
        pizzaPrice = parseInt(this.value);
        resetSize();
        resetToppings();
        enableAllowedToppings(this.dataset.allowedToppings);
        updatePrice();
      }
    });
  });

  // kita tambahkan event listenee u/ pilihan size, kaluu size kita pilih(checked) maka sizePrice akan diperbarui sesuai value dari size, dan pricenya akan diupdate
  sizes.forEach((size) => {
    size.addEventListener("change", function () {
      // mengecek apakah pizzanya sudah dipilih atau belum
      if (pizzaSelected && this.checked) {
        sizePrice = parseInt(this.value);
        updatePrice();
      }
    });
  });

  // kita tambahkan event listener u/ pilihan topping, jika kita memilih topping(checked) maka toppingsPrice nya akan diperbarui sesuai value dari topping, sedangkan jika kita unchecked topping maka toppingPrice akan dikurangi sesuai value yang kita unchecked tadi, dan price akan di update
  toppings.forEach((topping) => {
    topping.addEventListener("change", function () {
      // kondisi ini hanya akan dijalankan jika pizzaSelected bernilai true/ pizzanya sudah dipilih
      if (pizzaSelected) {
        if (this.checked) {
          toppingsPrice += parseInt(this.value);
        } else {
          toppingsPrice -= parseInt(this.value);
        }
        updatePrice();
      }
    });
  });
});

// mengatur default size menjadi medium lewat id
document.getElementById("medium").checked = true;
updatePrice();
